#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include "abh.h"

/* Oh so not threadsafe. */
static HV *known_keys;

MODULE = Acme::ABH		PACKAGE = Acme::ABH
PROTOTYPES: DISABLE

void
init_prng(seed)
	U32 seed
    CODE:
	MVP_hash_init_prng(seed);

SV *
build(entries = 0)
        U32 entries
    CODE:
        RETVAL = newSV_type(SVt_PV);
        sv_grow(RETVAL, sizeof(MVPHashTable *));
        SvPOK_on(RETVAL);
        SvCUR_set(RETVAL, sizeof(MVPHashTable *));
        MVP_hash_build((void *)SvPVX(RETVAL), sizeof(struct MVPpayload), entries);
    OUTPUT:
	RETVAL

void
demolish(hashtable)
        SV *hashtable
    CODE:
        MVP_hash_demolish((void *)SvPVX(hashtable));

SV *
shallow_copy(source)
        SV *source
    CODE:
        RETVAL = newSV_type(SVt_PV);
        sv_grow(RETVAL, sizeof(MVPHashTable *));
        SvPOK_on(RETVAL);
        SvCUR_set(RETVAL, sizeof(MVPHashTable *));
        MVP_hash_shallow_copy(*(void **)SvPVX(source), (void *)SvPVX(RETVAL));
    OUTPUT:
        RETVAL

void
insert(hashtable, key, value)
        SV *hashtable
        SV *key
        I32 value
    PREINIT:
        HE *he;
    CODE:
        if (!known_keys)
            known_keys = newHV();
        he = hv_fetch_ent(known_keys, key, 1, 0);
        MVP_hash_insert((void *)SvPVX(hashtable), HeKEY(he), value);

SV *
fetch(hashtable, key)
        SV *hashtable
        char *key
    PREINIT:
        struct MVPpayload *value;
    CODE:
        value = MVP_hash_fetch(*(void **)SvPVX(hashtable), key);
        if (value) {
            RETVAL = newSViv(value->payload);
        } else {
            RETVAL = &PL_sv_undef;
        }
    OUTPUT:
	RETVAL

SV *
lvalue_fetch(hashtable, key, value)
        SV *hashtable
        SV *key
        I32 value
    PREINIT:
        struct MVPpayload *entry;
        HE *he;
    CODE:
        if (!known_keys)
            known_keys = newHV();
        he = hv_fetch_ent(known_keys, key, 1, 0);
        entry = MVP_hash_lvalue_fetch((void *)SvPVX(hashtable), HeKEY(he));
        if (!entry) {
            Perl_croak_nocontext("lvalue_fetch %"SVf" returned NULL", key);
        }
        if (entry->key) {
            /* Seen previously. */
        } else {
            /* Insert. */
            entry->key = HeKEY(he);
            entry->payload = value;
        }
        RETVAL = newSViv(entry->payload);
    OUTPUT:
	RETVAL

void
delete(hashtable, key)
        SV *hashtable
        SV *key
    PREINIT:
        HE *he;
    CODE:
        if (!known_keys)
            known_keys = newHV();
        he = hv_fetch_ent(known_keys, key, 1, 0);
        MVP_hash_delete((void *)SvPVX(hashtable), HeKEY(he));

U32
count(hashtable)
        SV *hashtable
    CODE:
        RETVAL = MVP_hash_count(*(void **)SvPVX(hashtable));
    OUTPUT:
	RETVAL

U32
max_items(hashtable)
        SV *hashtable
    PREINIT:
        struct MVPHashTable *table = *(void **)SvPVX(hashtable);
    CODE:
        if (table->cur_items == 0 && table->max_items == 0) {
            RETVAL = 0;
        } else {
            RETVAL = MVP_calc_max_items(table);
        }
    OUTPUT:
	RETVAL


void
grow(hashtable, size)
        SV *hashtable
        U32 size
    CODE:
        MVP_hash_grow((void *)SvPVX(hashtable), size);

UV
fsck(hashtable, mode = 0)
        SV *hashtable
        U32 mode
    CODE:
        RETVAL = MVP_hash_fsck((void *)SvPVX(hashtable), mode);
    OUTPUT:
	RETVAL

U32
first(hashtable)
        SV *hashtable
    CODE:
        RETVAL = MVP_hash_first(*(void **)SvPVX(hashtable));
    OUTPUT:
	RETVAL

U32
next(hashtable, iterator)
        SV *hashtable
        U32 iterator
    CODE:
        RETVAL = MVP_hash_next(*(void **)SvPVX(hashtable), iterator);
    OUTPUT:
	RETVAL

U32
end(hashtable)
        SV *hashtable
    CODE:
        RETVAL = MVP_hash_end(*(void **)SvPVX(hashtable));
    OUTPUT:
	RETVAL

bool
at_end(hashtable, iterator)
        SV *hashtable
        U32 iterator
    CODE:
        RETVAL = MVP_hash_at_end(*(void **)SvPVX(hashtable), iterator);
    OUTPUT:
	RETVAL

void
current(hashtable, iterator)
        SV *hashtable
        U32 iterator
    PREINIT:
        struct MVPpayload *entry;
    PPCODE:
        entry = MVP_hash_current(*(void **)SvPVX(hashtable), iterator);
        if (!entry) {
            Perl_croak_nocontext("iterator %u returned NULL", iterator);
        }
        mPUSHp(entry->key, strlen(entry->key));
        mPUSHi(entry->payload);
        XSRETURN(2);
