/* A Better Hash.

A C implementation of https://github.com/martinus/robin-hood-hashing
by Martin Ankerl <http://martin.ankerl.com>

Better than what we have. Not better than his. His is hard to beat.

His design is for a Robin Hood hash (ie open addressing, Robin Hood probing)
with:

* a contiguous block of memory
* hash into 2**n slots
* instead of wrapping round from the end to the start of the array when
  probing, *actually* allocate some extra slots at the end, sufficient to cover
  the maximum permitted probe length
* store metadata for free/used (with the offset from the ideal slot) in a byte
  array immediately after the data slots
* store the offset in the top n bits of the byte, use the lower 8-n bits
  (possibly 0) to store (more) bits of the key's hash in the rest.
  (where n might be 0 - n is updated dynamically whenever a probe would overflow
   the currently permitted maxiumum)
  (so m bits of the hash are used to pick the ideal slot, and a different n are
   in the metadata, meaning that misses can be rejected more often)
* sentinel byte at the end of the metadata to cause the iterator to efficiently
  terminate
* setting max_items to 0 to force a resize before even trying another allocation
* when inserting and stealing a slot, move the next items up in bulk
  (ie don't implement it as "swap the new element with the evicted element and
  carry on inserting - the rest of the elements are already in a valid probe
  order, so just update *all* their metadata bytes, and then memmove them)

it's incredibly flexible (up to, automatically chosing whether to allocate
the value object inline in the hash, or indrected via a pointer), but
implemented as a C++ template.

Whereas we need something in C. Only for small structures, so they can always
go inline. And it turns out, our keys are always pointers, and easily "hashed"
(either because they are, because they point to something that caches its
hash value, or because we fake it and explicitly store the hash value.)

The only optimisation left is to change iterator metadata processing to be
word-at-a-time when possible. And even that might not be worth it.
*/

#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#undef NDEBUG
#include <assert.h>


typedef uint32_t MVPHashVal;

struct MVPpayload {
    const char *key;
    MVPHashVal hash_val;
    int32_t payload;
};

struct MVPHashTable {
    uint64_t salt;
    /* If cur_items and max_items are *both* 0 then we only allocated a control
       structure. All of the other entries in the struct are bogus, apart from
       entry_size, and calling many of the accessor methods for the hash will
       fail assertions.
       ("Doctor, Doctor, it hurts when I do this". "Well, don't do that then.")
       Iterators will return end immediately, fetch will fast track a not-found
       result, and insert will immediately allocate the default minimum size. */
    uint32_t cur_items;
    uint32_t max_items; /* hit this and we grow */
    uint8_t official_size_log2;
    uint8_t entry_size;
    /* This is the maximum probe distance we can use without updating the
       metadata. It might not *yet* be the maximum probe distance possible for
       the official_size. */
    uint8_t max_probe_distance;
    /* This is the maximum probe distance possible for the official size.
       We can (re)calcuate this from other values in the struct, but it's easier
       to cache it as we have the space. */
    uint8_t max_probe_distance_limit;
    uint8_t metadata_hash_bits;
    uint8_t key_right_shift;
};

typedef struct MVPHashTable MVPHashTable;
typedef uint32_t MVPHashIterator;

void MVP_hash_init_prng(uint64_t seed);

void MVP_hash_build(MVPHashTable **hashtable_p,
                    size_t entry_size,
                    uint32_t entries);

/* Frees the entire contents of the hash, leaving you just the hashtable itself,
   which you allocated (heap, stack, inside another struct, wherever) */
void MVP_hash_demolish(MVPHashTable **hashtable_p);
/* and then free memory if you allocated it */

void MVP_hash_shallow_copy(const MVPHashTable *source, MVPHashTable **dest_p);

/* UNCONDITIONALLY creates a new hash entry with the given key and value.
 * Doesn't check if the key already exists. Use with care. */
void MVP_hash_insert(MVPHashTable **hashtable_p,
                     const char *key,
                     int32_t value);

void *MVP_hash_fetch(MVPHashTable *hashtable,
                     const char *key);

void *MVP_hash_lvalue_fetch(MVPHashTable **hashtable_p,
                            const char *key);

void MVP_hash_delete(MVPHashTable **hashtable_p,
                     const char *key);

static inline int MVP_hash_is_empty(const MVPHashTable *hashtable) {
    return hashtable->cur_items == 0;
}

static inline uint32_t MVP_hash_count(const MVPHashTable *hashtable) {
    return hashtable->cur_items;
}

void MVP_hash_grow(MVPHashTable **hashtable_p, uint32_t wanted);

/* This is not part of the public API, and subject to change at any point.
   (possibly in ways that are actually incompatible but won't generate compiler
   warnings.) */
uint64_t MVP_hash_fsck(MVPHashTable **hashtable_p, uint32_t mode);

/* for now, I'm hard coding this.
   The assumption is that with a starting size of 8 (pointer sized) elements,
   load factor of 0.75 means max chain length of 6, so need five overrun slots,
   hence also 14 real metadata slots, plus 1 sentinel, hence two 8-byte words.
   I guess we could pump it up to 0.875, but I don't know what performance would
   be like.
   Possibly even we start it at 14/16, and on each doubling drop it by 1/16
   until it's down to 8/16 (ie 50%)
*/

/* See comments in hash_allocate_common (and elsewhere) before changing any of
   these, and test with assertions enabled. The current choices permit certain
   optimisation assumptions in parts of the code. */

#define ABH_LOAD_FACTOR 0.75
#define ABH_MIN_SIZE_BASE_2 3
#define ABH_INITIAL_BITS_IN_METADATA 5

/* These six are private. We need them out here for the inline functions.
   Use them. */
static inline uint32_t MVP_hash_kompromat(const struct MVPHashTable *hashtable) {
    assert(!(hashtable->cur_items == 0 && hashtable->max_items == 0));
    return (1 << (uint32_t)hashtable->official_size_log2) + hashtable->max_probe_distance - 1;
}
static inline uint32_t MVP_get_official_size(const struct MVPHashTable *hashtable) {
    assert(!(hashtable->cur_items == 0 && hashtable->max_items == 0));
    return 1 << (uint32_t)hashtable->official_size_log2;
}
static inline uint32_t MVP_calc_max_items(const struct MVPHashTable *hashtable) {
    return MVP_get_official_size(hashtable) * ABH_LOAD_FACTOR;
}

static inline char *MVP_hash_entries(struct MVPHashTable *hashtable) {
    assert(!(hashtable->cur_items == 0 && hashtable->max_items == 0));
    return (char *)hashtable - hashtable->entry_size;
}
static inline uint8_t *MVP_hash_metadata(struct MVPHashTable *hashtable) {
    assert(!(hashtable->cur_items == 0 && hashtable->max_items == 0));
    return (uint8_t *)hashtable + sizeof(struct MVPHashTable);
}
static inline const uint8_t *MVP_hash_metadata_const(const struct MVPHashTable *hashtable) {
    assert(!(hashtable->cur_items == 0 && hashtable->max_items == 0));
    return (const uint8_t *)hashtable + sizeof(struct MVPHashTable);
}

static inline MVPHashIterator MVP_hash_end(const MVPHashTable *hashtable) {
    return 0;
}

/* iterators are stored as unsigned values, metadata index plus one.
 * This is clearly an internal implementation detail. Don't cheat.
 */
static inline MVPHashIterator MVP_hash_next(const MVPHashTable *hashtable,
                                            MVPHashIterator iterator) {
    while (--iterator > 0) {
        if (MVP_hash_metadata_const(hashtable)[iterator - 1]) {
            return iterator;
        }
    }
    return 0;
}

static inline MVPHashIterator MVP_hash_first(const MVPHashTable *hashtable) {
    uint32_t iterator = MVP_hash_kompromat(hashtable);
    if (MVP_hash_metadata_const(hashtable)[iterator - 1]) {
        return iterator;
    }
    return MVP_hash_next(hashtable, iterator);
}

static inline void *MVP_hash_current(MVPHashTable *hashtable,
                                     MVPHashIterator iterator) {
    assert(MVP_hash_metadata(hashtable)[iterator - 1]);
    return MVP_hash_entries(hashtable) - hashtable->entry_size * (iterator - 1);
}

static inline bool MVP_hash_at_end(const MVPHashTable *hashtable,
                                   MVPHashIterator iterator) {
    return iterator == 0;
}
