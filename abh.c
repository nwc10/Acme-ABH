#include "abh.h"
#include "jfs64.h"

/* Adapted from the log_base2 function.
 * https://graphics.stanford.edu/~seander/bithacks.html#IntegerLogDeBruijn
 * -- Individually, the code snippets here are in the public domain
 * -- (unless otherwise noted)
 * This one was not marked with any special copyright restriction.
 * What we need is to round the value rounded up to the next power of 2, and
 * then the log base 2 of that. Don't call this with v == 0. */
static uint32_t round_up_log_base2(uint32_t v) {
    static const uint8_t MultiplyDeBruijnBitPosition[32] = {
        1, 10, 2, 11, 14, 22, 3, 30, 12, 15, 17, 19, 23, 26, 4, 31,
        9, 13, 21, 29, 16, 18, 25, 8, 20, 28, 24, 7, 27, 6, 5, 32
    };

    /* this rounds (v - 1) down to one less than a power of 2 */
    --v;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;

    return MultiplyDeBruijnBitPosition[(uint32_t)(v * 0x07C4ACDDU) >> 27];
}


unsigned int max_probe_distance = 255;

/* This is definately not threadsafe, and I don't intend to fix it to be so.
 */

static struct jfs64_state prng_state;

void MVP_hash_init_prng(uint64_t seed) {
    jfs64_raninit(&prng_state, seed);
}

static inline uint32_t get_allocated_items(const struct MVPHashTable *hashtable) {
    /* -1 because...
       probe distance of 1 is the correct bucket.
       hence for a value whose ideal slot is the last bucket, it's *in* the
       official allocation.
       probe distance of 2 is the first extra bucket beyond the official
       allocation
       probe distance of 255 is the 254th beyond the official allocation.
    */
    assert(!(hashtable->cur_items == 0 && hashtable->max_items == 0));
    return MVP_get_official_size(hashtable) + hashtable->max_probe_distance_limit - 1;
}

static inline void hash_demolish_internal(struct MVPHashTable *hashtable) {
    if (hashtable->cur_items == 0 && hashtable->max_items == 0) {
        free(hashtable);
        return;
    }

    size_t allocated_items = get_allocated_items(hashtable);
    size_t entries_size = hashtable->entry_size * allocated_items;
    char *start = (char *)hashtable - entries_size;
    free(start);
}

void MVP_hash_demolish(MVPHashTable **hashtable) {
    hash_demolish_internal(*hashtable);
    *hashtable = NULL;
}
/* and then free memory if you allocated it */

/* round up to a multiple of the pointer size. */
static inline size_t round_size_up(size_t wanted) {
    return (wanted - 1 + sizeof(void *)) & ~(sizeof(void *) - 1);
}

/* Relationship between "official" size (a power of two), how many buckets are
 * actually allocated, and the sequence of
 * ((max probe distances), (bits of hash stored in the metadata))
 * max probe distance is expressed as 1 .. $n
 * but the slot used is 0 .. $n - 1
 * metadata is stored as 1 .. $n
 * with 0 meaning "emtpy"
 *
 * This table shows some sizes we don't use. Smallest allocation is 8 + 5.
 *
 *                            max probe distance
 * buckets              <--   bits of extra hash    -->
 * allocated            6    5    4    3    2    1    0
 *    4 + 2             2 (from 3)
 *    8 + 5             3    6 (from 7)
 *   16 + 11            3    7   12 (from 15)
 *   32 + 23            3    7   15   24 (from 31)
 *   64 + 47            3    7   15   31   48 (from 63)
 *  128 + 95            3    7   15   31   63   96 (from 127)
 *  256 + 191           3    7   15   31   63  127  192 (from 255)
 *  512 + 254           3    7   15   31   63  127  255 (from 255)
 * 1024 + 254           3    7   15   31   63  127  255 (from 255)
 *
 * So for sizeof(long) == 4, the sentinel byte at the end of the allocated
 * metadata never gets reached if we shift metadata long-at-a-time.
 * For sizeof(long) == 8, it would get reached and shifted once if we have a
 * hash with 8 + 5 buckets, and start with 6 bits of hash in the metadata,
 * because we likely would first hit the short max probe distance of 3, and
 * reprocess the metadata bytes from ((2 bits probe distance), (6 bits hash))
 * to ((3 bits probe distance), (5 bits hash)), and if we do that 8-at-a-time,
 * we would (also) touch the sentinel byte stored 5 in.
 *
 * So, simple, we don't start with 6 bits of hash. Max is 5 bits. Which means
 * we can never grow the probe distance on our smallest 8 + 5 allocation - we
 * always hit the probe distance limit first and resize to a 16 + 11 hash.
 */

static inline struct MVPHashTable *
hash_allocate_common(uint8_t entry_size,
                     uint8_t official_size_log2) {
    uint32_t official_size = 1 << (uint32_t)official_size_log2;
    uint32_t max_items = official_size * ABH_LOAD_FACTOR;
    uint8_t max_probe_distance_limit;
    if (max_probe_distance < max_items) {
        max_probe_distance_limit = max_probe_distance;
    } else {
        max_probe_distance_limit = max_items;
    }
    size_t allocated_items = official_size + max_probe_distance_limit - 1;
    size_t entries_size = entry_size * allocated_items;
    size_t metadata_size = round_size_up(allocated_items + 1);

    size_t total_size
      = entries_size + sizeof (struct MVPHashTable) + metadata_size;
    assert(total_size == round_size_up(total_size));
    struct MVPHashTable *hashtable
        = (struct MVPHashTable *)((char *)malloc(total_size) + entries_size);

    hashtable->salt = jfs64_ranval(&prng_state);
    hashtable->official_size_log2 = official_size_log2;
    hashtable->max_items = max_items;
    hashtable->cur_items = 0;
    hashtable->metadata_hash_bits = ABH_INITIAL_BITS_IN_METADATA;
    /* ie 7: */
    uint8_t initial_probe_distance = (1 << (8 - ABH_INITIAL_BITS_IN_METADATA)) - 1;
    hashtable->max_probe_distance = max_probe_distance_limit > initial_probe_distance ? initial_probe_distance : max_probe_distance_limit;
    hashtable->max_probe_distance_limit = max_probe_distance_limit;
    uint8_t bucket_right_shift = 8 * sizeof(MVPHashVal) - official_size_log2;
    hashtable->key_right_shift = bucket_right_shift - hashtable->metadata_hash_bits;
    hashtable->entry_size = entry_size;

    uint8_t *metadata = (uint8_t *)(hashtable + 1);
    memset(metadata, 0, metadata_size);

    return hashtable;
}

static uint32_t base2_size_for_entries(uint32_t entries) {
    /* Minimum size we need to allocate, given the load factor. */
    uint32_t min_needed = entries * (1.0 / ABH_LOAD_FACTOR);
    if (min_needed < entries) {
        croak("Requested hash table size %" PRIu64 " is too large", entries);
    }
    uint32_t initial_size_base2 = round_up_log_base2(min_needed);
    if (initial_size_base2 < ABH_MIN_SIZE_BASE_2) {
        /* "Too small" - use our original defaults. */
        initial_size_base2 = ABH_MIN_SIZE_BASE_2;
    }
    return initial_size_base2;
}

void MVP_hash_build(MVPHashTable **hashtable,
                    size_t entry_size,
                    uint32_t entries) {
    if (UNLIKELY(entry_size == 0 || entry_size > 255
                 || entry_size & (sizeof(void *) - 1))) {
        croak("Hash table entry_size %"PRIu64" is invalid", entry_size);
    }

    if (!entries) {
        *hashtable = calloc(sizeof (struct MVPHashTable), 1);
        /* cur_items and max_items both 0 signals that we only allocated a
           control structure. */
        (*hashtable)->entry_size = entry_size;
    } else {
        *hashtable = hash_allocate_common(entry_size,
                                          base2_size_for_entries(entries));
    }
}

void MVP_hash_shallow_copy(const MVPHashTable *source, MVPHashTable **dest) {
    if (source->cur_items == 0 && source->max_items == 0) {
        struct MVPHashTable *empty = malloc(sizeof (struct MVPHashTable));
        memcpy(empty, source, sizeof(*empty));
        *dest = empty;
    } else {
        size_t allocated_items = get_allocated_items(source);
        size_t entries_size = source->entry_size * allocated_items;
        size_t metadata_size = round_size_up(allocated_items + 1);
        const char *start = (const char *)source - entries_size;
        size_t total_size
            = entries_size + sizeof (struct MVPHashTable) + metadata_size;
        char *target = malloc(total_size);
        memcpy(target, start, total_size);
        *dest = (struct MVPHashTable *)(target + entries_size);
    }
}

/* Unicode names (etc) are not under the control of an external attacker [:-)]
 * so we don't need a cryptographic hash function here. I'm assuming that
 * 32 bit FNV1a is good enough and fast enough to be useful. */
static inline MVPHashVal fnv1a(const char *key, size_t len) {
    const char *const end = key + len;
    MVPHashVal hash = 0x811c9dc5;
    while (key < end) {
        hash ^= *key++;
        hash *= 0x01000193;
    }
    return hash;
}

uint64_t MVP_hash_fsck(struct MVPHashTable **hashtable_p, uint32_t mode);

struct loop_state {
    char *entry_raw;
    uint8_t *metadata;
    unsigned int metadata_increment;
    unsigned int metadata_hash_mask;
    unsigned int probe_distance_shift;
    unsigned int max_probe_distance;
    unsigned int probe_distance;
    uint16_t entry_size;
};

static inline MVPHashVal salt_and_mix(const struct MVPHashTable *hashtable,
                                      MVPHashVal hash_val) {
    return (hashtable->salt ^ hash_val) * 0x9e3779b7;
}

/* This function turns out to be quite hot. We initialise looping on a *lot* of
   hashes. Removing a branch from this function reduced the instruction dispatch
   by over 0.1% for a non-trivial program (according to cachegrind. Sure, it
   doesn't change the likely cache misses, which is the first-order driver of
   performance.)

   Inspecting annotated compiler assembly output suggests that optimisers move
   the sections of this function around in the inlined code, and hopefully don't
   initialised any values until they are used. */

static inline struct loop_state create_loop_state(struct MVPHashTable *hashtable,
                                                  MVPHashVal hash_val) {
    /* "finalise" the hash by multiplying by the constant for Fibonacci bucket
       determination. */
    MVPHashVal mixed = salt_and_mix(hashtable, hash_val);
    struct loop_state retval;
    retval.entry_size = hashtable->entry_size;
    retval.metadata_increment = 1 << hashtable->metadata_hash_bits;
    retval.metadata_hash_mask = retval.metadata_increment - 1;
    retval.probe_distance_shift = hashtable->metadata_hash_bits;
    retval.max_probe_distance = hashtable->max_probe_distance;
    uint32_t used_hash_bits = mixed >> hashtable->key_right_shift;
    retval.probe_distance = retval.metadata_increment | (used_hash_bits & retval.metadata_hash_mask);
    uint32_t bucket = used_hash_bits >> hashtable->metadata_hash_bits;
    if (!hashtable->metadata_hash_bits) {
        assert(retval.probe_distance == 1);
        assert(retval.metadata_hash_mask == 0);
        assert(bucket == used_hash_bits);
    }

    retval.entry_raw = MVP_hash_entries(hashtable) - bucket * retval.entry_size;
    retval.metadata = MVP_hash_metadata(hashtable) + bucket;
    return retval;
}

static inline struct MVPpayload *hash_insert_internal(struct MVPHashTable *hashtable,
                                                      const char *key,
                                                      MVPHashVal hash_val) {
    if (UNLIKELY(hashtable->cur_items >= hashtable->max_items)) {
        MVP_hash_fsck(&hashtable, 5);
        croak("oops, hash_insert_internal has no space (%"PRIu32" >= %"PRIu32") when adding %s",
              hashtable->cur_items, hashtable->max_items, key);
    }

    struct loop_state ls = create_loop_state(hashtable, hash_val);

    while (1) {
        if (*ls.metadata < ls.probe_distance) {
            /* this is our slot. occupied or not, it is our rightful place. */

            if (*ls.metadata == 0) {
                /* Open goal. Score! */
            } else {
                /* make room. */

                /* Optimisation first seen in Martin Ankerl's implementation -
                   we don't need actually implement the "stealing" by swapping
                   elements and carrying on with insert. The invariant of the
                   hash is that probe distances are never out of order, and as
                   all the following elements have probe distances in order, we
                   can maintain the invariant just as well by moving everything
                   along by one. */
                uint8_t *find_me_a_gap = ls.metadata;
                uint8_t old_probe_distance = *ls.metadata;
                do {
                    uint32_t new_probe_distance = ls.metadata_increment + old_probe_distance;
                    if (new_probe_distance >> ls.probe_distance_shift == ls.max_probe_distance) {
                        /* Optimisation from Martin Ankerl's implementation:
                           setting this to zero forces a resize on any insert,
                           *before* the actual insert, so that we never end up
                           having to handle overflow *during* this loop. This
                           loop can always complete. */
                        hashtable->max_items = 0;
                    }
                    /* a swap: */
                    old_probe_distance = *++find_me_a_gap;
                    *find_me_a_gap = new_probe_distance;
                } while (old_probe_distance);

                uint32_t entries_to_move = find_me_a_gap - ls.metadata;
                size_t size_to_move = ls.entry_size * entries_to_move;
                /* When we had entries *ascending* in memory, this was
                 * memmove(entry_raw + hashtable->entry_size, entry_raw,
                 *         size_to_move);
                 * because we point to the *start* of the block of memory we
                 * want to move, and we want to move it one "entry" forwards.
                 * `entry_raw` is still a pointer to where we want to make free
                 * space, but what want to do now is move everything at it and
                 * *before* it downwards.
                 */
                char *dest = ls.entry_raw - size_to_move;
                memmove(dest, dest + ls.entry_size, size_to_move);
            }

            /* However, we can still exceed the new (lower) probe distances
               that we initially set.
               Optimisation from Martin Ankerl's implementation:
               setting this to zero forces a resize on any insert, *before*
               the actual insert, so that we never end up having to handle
               overflow *during* this loop. This loop can always
               complete. */
            if (ls.probe_distance >> ls.probe_distance_shift == ls.max_probe_distance) {
                if (hashtable->cur_items >= hashtable->max_items) {
                    /* We've hit the load factor limit. The next insert
                       would grow the hash anyway, so don't confuse things
                       by attempting to trigger a metadata shuffle. */
                } else {
                    hashtable->max_items = 0;
                }
            }

            /* The same test and optimisation as in the "make room" loop - we're
               about to insert something at the (current) max_probe_distance, so
               signal to the next insertion that it needs to take action first.
            */
            if (ls.probe_distance == hashtable->max_probe_distance) {
                if (hashtable->cur_items >= hashtable->max_items) {
                    /* We've hit the load factor limit. The next insert would
                       grow the hash anyway, so don't confuse things by
                       attempting to trigger a metadata shuffle. */
                } else {
                    hashtable->max_items = 0;
                }
            }

            ++hashtable->cur_items;

            *ls.metadata = ls.probe_distance;
            struct MVPpayload *entry = (struct MVPpayload *) ls.entry_raw;
            entry->key = NULL;
            return entry;
        }
        else if (*ls.metadata == ls.probe_distance && key) {
            /* key is NULL when called from maybe_grow_hash. For that case we
               know that we can't find any duplicates already in the hash. */
            struct MVPpayload *entry = (struct MVPpayload *) ls.entry_raw;
            if (entry->hash_val == hash_val && 0 == strcmp(entry->key, key)) {
                return entry;
            }
        }
        ls.probe_distance += ls.metadata_increment;
        ++ls.metadata;
        ls.entry_raw -= ls.entry_size;
        assert(ls.probe_distance < (hashtable->max_probe_distance + 1) * ls.metadata_increment);
        assert(ls.metadata < MVP_hash_metadata(hashtable) + MVP_get_official_size(hashtable) + MVP_calc_max_items(hashtable));
        assert(ls.metadata < MVP_hash_metadata(hashtable) + MVP_get_official_size(hashtable) + 256);
    }
}

static struct MVPHashTable *grow_hash(struct MVPHashTable *hashtable_orig, uint8_t official_size_log2);

static struct MVPHashTable *
maybe_grow_hash(struct MVPHashTable *hashtable) {
    if (UNLIKELY(hashtable->cur_items == 0 && hashtable->max_items == 0)) {
        struct MVPHashTable *hashtable_new
            = hash_allocate_common(hashtable->entry_size, ABH_MIN_SIZE_BASE_2);
        free(hashtable);
        return hashtable_new;
    }

    /* hashtable->max_items may have been set to 0 to trigger a call into this
       function. */
    const uint32_t max_items = MVP_calc_max_items(hashtable);
    const uint32_t max_probe_distance = hashtable->max_probe_distance;
    const uint32_t max_probe_distance_limit = hashtable->max_probe_distance_limit;

    /* We can hit both the probe limit and the max items on the same insertion.
       In which case, upping the probe limit isn't going to save us :-)
       But if we hit the probe limit max (even without hitting the max items)
       then we don't have more space in the metadata, so we're going to have to
       grow anyway. */
    if (hashtable->cur_items < max_items
        && max_probe_distance < max_probe_distance_limit) {
        /* We hit the probe limit, not the max items count. */
        uint32_t new_probe_distance = 1 + 2 * (uint32_t) max_probe_distance;
        if (new_probe_distance > max_probe_distance_limit) {
#ifdef DEBUG_SPAM
            fprintf(stderr, "grow %p %u => %u (limited to %u size %u)\n",
                    hashtable, max_probe_distance, new_probe_distance,
                    max_probe_distance_limit, hashtable->official_size_log2);
#endif
            new_probe_distance = max_probe_distance_limit;
        } else {
#ifdef DEBUG_SPAM
            fprintf(stderr, "grow %p %u => %u (limit %u, size %u)\n",
                    hashtable, max_probe_distance, new_probe_distance,
                    max_probe_distance_limit, hashtable->official_size_log2);
#endif
        }

        uint8_t *metadata = MVP_hash_metadata(hashtable);
        uint32_t in_use_items = MVP_get_official_size(hashtable) + max_probe_distance;
        size_t metadata_size = round_size_up(in_use_items + 1);
        size_t loop_count = metadata_size / sizeof(unsigned long);
        unsigned long *p = (unsigned long *) metadata;
        /* right shift each byte by 1 bit, clearing the top bit. */
        do {
            /* 0x7F7F7F7F7F7F7F7F on 64 bit systems, 0x7F7F7F7F on 32 bit,
               but without using constants or shifts larger than 32 bits, or
               the preprocessor. (So the compiler checks all code everywhere.)
               Will break on a system with 128 bit longs. */
            *p = (*p >> 1) & (0x7F7F7F7FUL | (0x7F7F7F7FUL << (4 * sizeof(long))));
            ++p;
        } while (--loop_count);
        assert(hashtable->metadata_hash_bits);
        --hashtable->metadata_hash_bits;
        ++hashtable->key_right_shift;
        hashtable->max_probe_distance = new_probe_distance;
        /* Reset this to its proper value. */
        hashtable->max_items = max_items;
        return NULL;
    }

    return grow_hash(hashtable, hashtable->official_size_log2 + 1);
}

static struct MVPHashTable *
grow_hash(struct MVPHashTable *hashtable_orig, uint8_t official_size_log2) {
    uint32_t entries_in_use =  MVP_hash_kompromat(hashtable_orig);
    char *entry_raw_orig = MVP_hash_entries(hashtable_orig);
    uint8_t *metadata_orig = MVP_hash_metadata(hashtable_orig);
    const uint8_t entry_size = hashtable_orig->entry_size;

    struct MVPHashTable * hashtable
        = hash_allocate_common(entry_size, official_size_log2);

    char *entry_raw = entry_raw_orig;
    uint8_t *metadata = metadata_orig;
    uint32_t bucket = 0;
    while (bucket < entries_in_use) {
        if (*metadata) {
            struct MVPpayload *old_entry = (struct MVPpayload *) entry_raw;
            struct MVPpayload *new_entry
                = hash_insert_internal(hashtable, NULL, old_entry->hash_val);
            assert(new_entry->key == NULL);
            *new_entry = *old_entry;
            if (!hashtable->max_items) {
                /* Probably we hit the probe limit.
                   But it's just possible that one actual "grow" wasn't
                   enough. */
                struct MVPHashTable *new_hashtable = maybe_grow_hash(hashtable);
                if (new_hashtable) {
#ifdef DEBUG_SPAM
                    fprintf(stderr, "new hashtable %p => %p\n",
                            hashtable, new_hashtable);
#endif
                    hashtable = new_hashtable;
                } else {
#ifdef DEBUG_SPAM
                    fprintf(stderr, "expanded probe distance %p %u => %p %u\n",
                            hashtable_orig, hashtable_orig->max_probe_distance,
                            hashtable, max_probe_distance);
#endif
                }
            }
        }
        ++bucket;
        ++metadata;
        entry_raw -= entry_size;
    }
#ifdef DEBUG_SPAM
    fprintf(stderr, "grew %u to %u\n",
            hashtable_orig->official_size_log2,
            hashtable->official_size_log2);
#endif
    assert(hashtable->max_items);
    hash_demolish_internal(hashtable_orig);
    return hashtable;
}

void MVP_hash_grow(MVPHashTable **hashtable_p, uint32_t wanted) {
    if (!wanted) {
        croak("MVP_hash_grow called with wanted == 0");
    }
    MVPHashTable *hashtable = *hashtable_p;
    uint32_t rounded_up = base2_size_for_entries(wanted);

    if (UNLIKELY(hashtable->cur_items == 0 && hashtable->max_items == 0)) {
        struct MVPHashTable *hashtable_new
            = hash_allocate_common(hashtable->entry_size, rounded_up);
        free(hashtable);
        *hashtable_p = hashtable_new;
        return;
    }

    if (rounded_up <= hashtable->official_size_log2) {
        return;
    }

    *hashtable_p = grow_hash(hashtable, rounded_up);
    return;
}

void *MVP_hash_lvalue_fetch(MVPHashTable **hashtable_p,
                            const char *key) {
    MVPHashTable *hashtable = *hashtable_p;
    if (0) {
        /* This code is now unreachable for MVP hash, but for some derived
           versions where we don't need to set entry_size, it's still useful
           to have this. */
        if (UNLIKELY(hashtable->entry_size == 0)) {
            croak("Hash table entry_size not set");
        }
        if (UNLIKELY(hashtable->entry_size > 255 || hashtable->entry_size & (sizeof(void *) - 1))) {
            croak("Hash table entry_size %u is invalid", hashtable->entry_size);
        }
        uint32_t initial_size_base2 = ABH_MIN_SIZE_BASE_2;
        hashtable = hash_allocate_common(hashtable->entry_size,
                                       initial_size_base2);
    }
    else if (UNLIKELY(hashtable->cur_items >= hashtable->max_items)) {
        /* We should avoid growing the hash if we don't need to.
         * It's expensive, and for hashes with iterators, growing the hash
         * invalidates iterators. Which is buggy behaviour if the fetch doesn't
         * need to create a key. */
        void *entry = MVP_hash_fetch(hashtable, key);
        if (entry) {
            return entry;
        }

        struct MVPHashTable *new_hashtable = maybe_grow_hash(hashtable);
        if (new_hashtable) {
            /* We could unconditionally assign this, but that would mean CPU
               cache writes even when it was unchanged, and the address of
               hashtable will not be in the same cache lines as we are writing
               for the hash internals, so it will churn the write cache. */
            *hashtable_p = hashtable = new_hashtable;
        }
    }
    MVPHashVal hash_val = fnv1a(key, strlen(key));
    struct MVPpayload *new_entry
        = hash_insert_internal(hashtable, key, hash_val);
    if (!new_entry->key) {
        new_entry->hash_val = hash_val;
    }
    return new_entry;
}


/* UNCONDITIONALLY creates a new hash entry with the given key and value.
 * Doesn't check if the key already exists. Use with care. */
void MVP_hash_insert(MVPHashTable **hashtable,
                     const char *key,
                     int32_t value) {
    struct MVPpayload *new_entry = MVP_hash_lvalue_fetch(hashtable, key);
    if (new_entry->key) {
        MVPHashVal hash_val = fnv1a(key, strlen(key));
        if (value != new_entry->payload) {
            /* definately XXX - what should we do here? */
            croak("insert conflict, %s is %u, %i != %i", key, hash_val, value, new_entry->payload);
        } else {
            croak("insert duplicate, %s is %u, %i == %i", key, hash_val, value, new_entry->payload);
        }
    } else {
        new_entry->key = key;
        new_entry->payload = value;
    }
}

void *MVP_hash_fetch(MVPHashTable *hashtable,
                     const char *key) {
    if (UNLIKELY(MVP_hash_is_empty(hashtable))) {
        return NULL;
    }

    MVPHashVal hash_val = fnv1a(key, strlen(key));
    struct loop_state ls = create_loop_state(hashtable, hash_val);

    while (1) {
        if (*ls.metadata == ls.probe_distance) {
            struct MVPpayload *entry = (struct MVPpayload *) ls.entry_raw;
            if (entry->hash_val == hash_val && 0 == strcmp(entry->key, key)) {
                return entry;
            }
        }
        /* There's a sentinel at the end. This will terminate: */
        else if (*ls.metadata < ls.probe_distance) {
            /* So, if we hit 0, the bucket is empty. "Not found".
               If we hit something with a lower probe distance then...
               consider what would have happened had this key been inserted into
               the hash table - it would have stolen this slot, and the key we
               find here now would have been displaced further on. Hence, the
               key we seek can't be in the hash table. */
            return NULL;
        }
        ls.probe_distance += ls.metadata_increment;
        ++ls.metadata;
        ls.entry_raw -= ls.entry_size;
        assert(ls.probe_distance < (hashtable->max_probe_distance + 2) * ls.metadata_increment);
        assert(ls.metadata < MVP_hash_metadata(hashtable) + MVP_get_official_size(hashtable) + MVP_calc_max_items(hashtable));
        assert(ls.metadata < MVP_hash_metadata(hashtable) + MVP_get_official_size(hashtable) + 256);
    }
}

void MVP_hash_delete(MVPHashTable **hashtable_p,
                     const char *key) {
    struct MVPHashTable *hashtable = *hashtable_p;
    if (UNLIKELY(MVP_hash_entries(hashtable) == NULL)) {
        return;
    }
    MVPHashVal hash_val = fnv1a(key, strlen(key));
    struct loop_state ls = create_loop_state(hashtable, hash_val);
    while (1) {
        if (*ls.metadata == ls.probe_distance) {
            struct MVPpayload *entry = (struct MVPpayload *) ls.entry_raw;
            if (entry->hash_val == hash_val && 0 == strcmp(entry->key, key)) {
                /* Target acquired. */

                uint8_t *metadata_target = ls.metadata;
                /* Look at the next slot */
                uint8_t old_probe_distance = metadata_target[1];
                /* Without this, gcc seemed always to want to recalculate this
                 for each loop iteration. Also, expressing this only in terms
                 of ls.metadata_increment avoids 1 load (albeit from cache) */
                const uint8_t can_move = 2 * ls.metadata_increment;
                while (old_probe_distance >= can_move) {
                    /* OK, we can move this one. */
                    *metadata_target = old_probe_distance - ls.metadata_increment;
                    /* Try the next one, etc */
                    ++metadata_target;
                    old_probe_distance = metadata_target[1];
                }
                /* metadata_target now points to the metadata for the last thing
                   we did move. (possibly still our target). */

                uint32_t entries_to_move = metadata_target - ls.metadata;
                if (entries_to_move) {
                    size_t size_to_move = ls.entry_size * entries_to_move;
                    /* When we had entries *ascending* in memory, this was
                     * memmove(entry_raw, entry_raw + hashtable->entry_size, ,
                     *         size_to_move);
                     * because we point to the *start* of the block of memory we
                     * want to move, and we want to move the block one "entry"
                     * backwards.
                     * `entry_raw` is still a pointer to the entry that we need
                     * to ovewrite, but now we need to move everything *before*
                     * it upwards to close the gap.
                     */
                    memmove(ls.entry_raw - size_to_move + ls.entry_size,
                            ls.entry_raw - size_to_move,
                            size_to_move);
                }
                /* and this slot is now emtpy. */
                *metadata_target = 0;
                --hashtable->cur_items;

                if (hashtable->max_items == 0
                    && hashtable->cur_items < hashtable->max_probe_distance) {
                    /* So this is a fun corner case...

                       For empty hashes we have a space optimisation to (not)
                       allocate "8" (ie 13) slots initially. Instead we only
                       allocate a control structure. However most of the
                       metadata in that is stored log base 2 (another size
                       optimisation) hence *it* can't store zero. So we mark
                       this case by setting both hashtable->cur_items == 0 &&
                       hashtable->max_items == 0
                       `max_items` zero triggers immediate allocation on any
                       insert ("no questions asked") and `cur_items` zero is the
                       true state.

                       The assumption of that commit was that it was impossible
                       for the control structure to be in that (zero,zero) state
                       as soon as any insert happened, because after that
                       max_items would only be set to zero if an insert flagged
                       up that the `max_probe_distance` might overflow on the
                       *next* insert (ie trigger that immediate allocation), and
                       if there are items allocated, then (obviously)
                       `cur_items` isn't zero.

                       What I missed was that a sequence of inserts followed by
                       a sequence of deletes can reach (zero,zero). If the
                       *last* insert hits the `max_probe_distance` limit, then
                       it sets `max_items` to zero so that the next insert will
                       allocate.

                       But what if there is no next insert?

                       Then nothing resets the `max_items`, and it remains as
                       zero.

                       And if the only write actions that happen on the hash
                       after this are to delete each entry in turn, then
                       eventually `cur_items` reaches zero too (accurately), and
                       then the "special state" flags are accidentally recreated
                       but not with the "special state" memory layout.

                       (And with enough debugging enabled the assignment to
                        `hashtable->last_delete_at` just below fails an assertion
                        in `MVM_str_hash_metadata`)

                       So clearly we need to unwind the "immediate allocation"
                       flag.

                       We certainly can't do it on *any* delete

                       We can't actually do it on *any* delete that drops a
                       probe distance below the limit because (this one is
                       subtle) worst case it's possible for
                       a) a hash to be in the state where several entries have
                          probe distances one below the threshold
                       b) a *single* insert causes 2+ of these to move up
                          (the "make room" code in insert)
                          (or 1 to move up, *and* the new insert to be at the
                           the trigger distance)
                           which sets the flag
                       c) but the immediately subsequent deletion causes only
                           *one* of these to drop back below the max.

                       The really conservative approach would only be to reset
                       if the hash is about to hit zero items.

                       But I believe that the earliest we can be *sure* that no
                       chain is longer than the limit is when the *total*
                       entries in the hash are less than that limit. Because (of
                       course), the worst case is that they are all in a row
                       contesting the same ideal bucket, and the highest probe
                       distance has to be less than the limit.

                       So here we are: */

                    hashtable->max_items = MVP_calc_max_items(hashtable);
                }

                /* Job's a good 'un. */
                return;
            }
        }
        /* There's a sentinel at the end. This will terminate: */
        else if (*ls.metadata < ls.probe_distance) {
            /* So, if we hit 0, the bucket is empty. "Not found".
               If we hit something with a lower probe distance then...
               consider what would have happened had this key been inserted into
               the hash table - it would have stolen this slot, and the key we
               find here now would have been displaced further on. Hence, the
               key we seek can't be in the hash table. */
            return;
        }
        ls.probe_distance += ls.metadata_increment;
        ++ls.metadata;
        ls.entry_raw -= ls.entry_size;
        assert(ls.probe_distance < (hashtable->max_probe_distance + 2) * ls.metadata_increment);
        assert(ls.metadata < MVP_hash_metadata(hashtable) + MVP_get_official_size(hashtable) + MVP_calc_max_items(hashtable));
        assert(ls.metadata < MVP_hash_metadata(hashtable) + MVP_get_official_size(hashtable) + 256);
    }
}


/* This is not part of the public API, and subject to change at any point.
   (possibly in ways that are actually incompatible but won't generate compiler
   warnings.) */
uint64_t MVP_hash_fsck(MVPHashTable **hashtable_p, uint32_t mode) {
    MVPHashTable *hashtable = *hashtable_p;
    const char *prefix_hashes = mode & 1 ? "# " : "";
    uint32_t display = (mode >> 1) & 3;
    uint64_t errors = 0;
    uint64_t seen = 0;

    if (hashtable->cur_items == 0 && hashtable->max_items == 0) {
        return 0;
    }

    uint32_t allocated_items = get_allocated_items(hashtable);
    const uint8_t metadata_hash_bits = hashtable->metadata_hash_bits;
    char *entry_raw = MVP_hash_entries(hashtable);
    uint8_t *metadata = MVP_hash_metadata(hashtable);
    uint32_t bucket = 0;
    int64_t prev_offset = 0;
    uint8_t bucket_right_shift
      = hashtable->key_right_shift + hashtable->metadata_hash_bits;
    while (bucket < allocated_items) {
        if (!*metadata) {
            /* empty slot. */
            prev_offset = 0;
            if (display == 2) {
                fprintf(stderr, "%s%3X\n", prefix_hashes, bucket);
            }
        } else {
            ++seen;

            struct MVPpayload *entry = (struct MVPpayload *) entry_raw;
            MVPHashVal mixed = salt_and_mix(hashtable, entry->hash_val);

            uint32_t ideal_bucket = mixed >> bucket_right_shift;
            int64_t offset = 1 + bucket - ideal_bucket;
            uint32_t actual_bucket = *metadata >> metadata_hash_bits;
            char wrong_bucket = offset == actual_bucket ? ' ' : '?';
            char wrong_order;
            if (offset < 1) {
                wrong_order = '<';
            } else if (offset > hashtable->max_probe_distance) {
                wrong_order = '>';
            } else if (offset > prev_offset + 1) {
                wrong_order = '!';
            } else {
                wrong_order = ' ';
            }
            int error_count = (wrong_bucket != ' ') + (wrong_order != ' ');

            if (display == 2 || (display == 1 && error_count)) {
                fprintf(stderr, "%s%3X%c%3"PRIx64"%c%08X %s\n", prefix_hashes,
                        bucket, wrong_bucket, offset, wrong_order,
                        mixed, entry->key);
                errors += error_count;
            }
            prev_offset = offset;
        }
        ++bucket;
        ++metadata;
        entry_raw -= hashtable->entry_size;
    }
    if (*metadata != 0) {
        ++errors;
        if (display) {
            fprintf(stderr, "%s    %02x!\n", prefix_hashes, *metadata);
        }
    }
    if (seen != hashtable->cur_items) {
        ++errors;
        if (display) {
            fprintf(stderr, "%s %"PRIx64"u != %"PRIx32"u \n",
                    prefix_hashes, seen, hashtable->cur_items);
        }
    }

    return errors;
}
