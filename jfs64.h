/* This is the "A small noncryptographic PRNG" by Bob Jenkins, later given the
   name JFS64.
   http://burtleburtle.net/bob/rand/smallprng.html
   "I wrote this PRNG. I place it in the public domain." */

#include <stdint.h>

struct jfs64_state {
    uint64_t a;
    uint64_t b;
    uint64_t c;
    uint64_t d;
};

#define jfs64_rot(x,k) (((x)<<(k))|((x)>>(64-(k))))

static inline uint64_t jfs64_ranval( struct jfs64_state *x ) {
    uint64_t e = x->a - jfs64_rot(x->b, 7);
    x->a = x->b ^ jfs64_rot(x->c, 13);
    x->b = x->c + jfs64_rot(x->d, 37);
    x->c = x->d + e;
    x->d = e + x->a;
    return x->d;
}

static inline void jfs64_raninit( struct jfs64_state *x, uint64_t seed ) {
    uint64_t i;
    x->a = 0xf1ea5eed;
    x->b = x->c = x->d = seed;
    for (i=0; i<20; ++i) {
        (void)jfs64_ranval(x);
    }
}
