package Acme::ABH;

use 5.008; # But probably actually needs something more recent.
use strict;
use warnings;

use parent 'Exporter';
our @EXPORT_OK = qw(init_prng build demolish insert fetch lvalue_fetch delete
                    max_items count grow fsck
                    first next current end at_end shallow_copy);

our $VERSION = '0.01';

require XSLoader;
XSLoader::load('Acme::ABH', $VERSION);

'Have the appropriate amount of fun';

__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Acme::ABH - A Better Hash

=head1 SYNOPSIS

  use Acme::ABH;
  blah blah blah blah blah

=head1 DESCRIPTION

A test rig.

=head1 AUTHOR

Nicholas Clark <nick@ccl4.org>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2020 by Nicholas Clark.

A C re-implenmentation of design and C++ implementation of Robin Hood hashing
by Martin Ankerl L<http://martin.ankerl.com>
at L<https://github.com/martinus/robin-hood-hashing>

This program is free software; you can redistribute it and/or modify
it under the terms of either of

=over 4

=item *

The same terms as MoarVM, so
I<The Artistic License 2.0 L<https://opensource.org/licenses/Artistic-2.0>>

=item *

The same terms as Perl 5 (Version 5.32.0 or later), so either of

=over 4

I<GPL 2.0 or later <https://opensource.org/licenses/GPL-2.0>>

=over 4

I<Artistic 1.0 <https://opensource.org/licenses/Artistic-1.0>>

=back

=back

As ever, the lawyers like to remind you

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.

It's written in C. I'm confident that it relies on undefined behaviour.
You've been warned.

=cut
