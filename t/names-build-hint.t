#!usr/bin/perl
use strict;
use warnings;

use Test::More;
BEGIN {
    use_ok('Acme::ABH', qw(init_prng build demolish count insert fetch delete
                           fsck));
};

use FindBin qw($Bin);

my $seed;

if (@ARGV) {
    $seed = shift;
    diag(__FILE__ . " reseeding with $seed");
} else {
    $seed = int rand 1 + 0xFFFFFFFF;
    diag(__FILE__ . " seeding with $seed");
}

init_prng($seed);

my %to_cp;
open my $fh, '<', "$Bin/Names";
while (<$fh>) {
    chomp;
    my ($cph, $name) = /^ *([A-F0-9]+) +([-A-Z0-9 ]+)/;
    die $_
        unless defined $name;
    my $cp = hex $cph;
    # We know U-0000 is not in this file;
    die $_
        unless $cp;

    $to_cp{$name} = $cp;
}

my @names = keys %to_cp;

my $hash = build(scalar @names);

for my $name (@names) {
    insert($hash, $name, $to_cp{$name});
}

for my $name (@names) {
    is(fetch($hash, $name), $to_cp{$name}, "correct value for $name");
}
is(fsck($hash, 3), 0, "fsck reports no errors");

while (@names) {
    is(count($hash), scalar @names, "hash has correct entry count");
    my $name = shift @names;
    is(fetch($hash, $name), $to_cp{$name}, "correct value for $name");
    &delete($hash, $name);
    is(fetch($hash, $name), undef, "$name now deleted");
}
is(count($hash), 0, "hash is empty");

demolish($hash);
done_testing();
