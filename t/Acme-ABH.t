#!perl
use strict;
use warnings;

use Test::More;
BEGIN {
    use_ok('Acme::ABH',
           qw(init_prng build demolish count insert fetch lvalue_fetch delete
              fsck first next current end at_end shallow_copy max_items grow));
}

my $seed;

if (@ARGV) {
    $seed = shift;
    diag(__FILE__ . " reseeding with $seed");
} else {
    $seed = int rand 1 + 0xFFFFFFFF;
    diag(__FILE__ . " seeding with $seed");
}

init_prng($seed);

my $hash = build();
is(fsck($hash, 0), 0, "fsck reports no errors");
is(max_items($hash), 0, "max items is 0");
my $clone = shallow_copy($hash);
is(max_items($clone), 0, "cloned max items is 0");
is(fsck($clone, 0), 0, "fsck reports no errors on clone");
demolish($hash);

$hash = build();
cmp_ok(length $hash, '>', 0, "hash structure is not empty");
is(count($hash), 0, "hash is empty");
is(max_items($hash), 0, "max items is 0");
is(fetch($hash, 'pi'), undef, "first key not yet found");
is(fetch($hash, 'e'), undef, "second key not yet found");

is(eval { insert($hash, 'pi', 4); 3.14 }, 3.14, "first insert does not throw");
is($@, "", "no error message");
is(count($hash), 1, "hash has 1 entry");
is(fetch($hash, 'pi'), 4, "first key found");
is(fetch($hash, 'e'), undef, "second key not yet found");
is(fsck($hash, 3), 0, "fsck reports no errors");
cmp_ok(max_items($hash), '>', 0, "max items > 0");

is(eval { insert($hash, 'e', 2); 2.71 }, 2.71, "second insert does not throw");
is($@, "", "no error message");
is(count($hash), 2, "hash has 2 entries");
is(fetch($hash, 'pi'), 4, "first key found");
is(fetch($hash, 'e'), 2, "second key found");
is(fsck($hash, 3), 0, "fsck reports no errors");

# This is somewhat a hack - we shouldn't really be inserting the same key
is(eval { insert($hash, 'pi', 4); 3.14 }, undef, "repeat insert throws");
like($@, qr/insert duplicate/, "error message");
diag($@);
is(count($hash), 2, "hash still has 2 entries");

is(eval { insert($hash, 'pi', 3); 3.14 }, undef, "contradictory repeat insert throws");
like($@, qr/insert conflict/, "error message");
diag($@);
is(count($hash), 2, "hash still has 2 entries");

demolish($hash);

cmp_ok(length $clone, '>', 0, "hash structure is not empty");
is(count($clone), 0, "hash is empty");
is(fetch($clone, 'pi'), undef, "first key not found");
is(fetch($clone, 'e'), undef, "second key not found");
is(fetch($clone, 'perl'), undef, "third key not yet found");
is(eval { insert($clone, 'pi**2', 10); 9.87 }, 9.87, "third insert does not throw");
is($@, "", "no error message");
is(count($clone), 1, "hash has 1 entry");
is(fetch($clone, 'pi**2'), 10, "third key found");
is(fsck($clone, 3), 0, "fsck reports no errors");

demolish($clone);

my @unicode = (
    [SPACE => 0x20],
    ['EXCLAMATION MARK' => 0x21],
    ['QUOTATION MARK' => 0x22],
    ['NUMBER SIGN' => 0x23],
    ['DOLLAR SIGN' => 0x24],
    ['PERCENT SIGN' => 0x25],
    ['AMPERSAND' => 0x26],
    ['APOSTROPHE' => 0x27],
);

$hash = build();
is(max_items($hash), 0, "max items is 0");
grow($hash, 1);
my $max = max_items($hash);
isnt($max, 0, "max items is no longer 0");
isnt($max, 1, "min hash size is larger than 1");
# This is a bit chummy with the implementation. We know that the minimum size
# isn't enough for all 8.
cmp_ok($max, '<', scalar @unicode, "max items is less than " . @unicode);

grow($hash, scalar @unicode);

my $new_max = max_items($hash);
cmp_ok($new_max, '>', $max, "hash expanded");
cmp_ok($new_max, '>=', scalar @unicode, "max items is sufficient for " . @unicode);


for (@unicode) {
    my ($name, $cp) = @$_;
    insert($hash, $name, $cp);
    is(fetch($hash, $name), $cp, "$name inserted correctly");
}
is(fsck($hash, 5), 0, "fsck reports no errors");

$clone = shallow_copy($hash);

is(count($hash), 8, "hash has 8 entries");
is(fetch($hash, 'QUOTATION MARK'), 34, "quotation mark is found");
&delete($hash, 'QUOTATION MARK');
is(fetch($hash, 'QUOTATION MARK'), undef, "quotation mark is no longer found");
is(count($hash), 7, "hash has 7 entries");
is(fetch($hash, 'EXCLAMATION MARK'), 33, "exclamation mark is found");
&delete($hash, 'EXCLAMATION MARK');
is(fetch($hash, 'EXCLAMATION MARK'), undef, "exclamation mark is no longer found");
is(count($hash), 6, "hash has 6 entries");
is(fsck($hash, 5), 0, "fsck reports no errors");
demolish($hash);

$hash = build();
my $count;
my %want;
for (@unicode) {
    my ($name, $cp) = @$_;
    $want{$name} = $cp;
    my $got = lvalue_fetch($hash, $name, $cp);
    is($got, $cp, "$name inserted correctly with lvalue_fetch");
    is(fetch($hash, $name), $cp, "$name inserted correctly");
    is(fsck($hash, 3), 0, "fsck reports no errors");
    is(count($hash), ++$count, "hash count correct at $name");

    $got = lvalue_fetch($hash, $name, -$cp);
    is($got, $cp, "$name not overwritten with lvalue_fetch");
    is(fetch($hash, $name), $cp, "$name inserted correctly");
    is(fsck($hash, 3), 0, "fsck reports no errors");
    is(count($hash), $count, "hash count still correct at $name");

    is(fetch($clone, $name), $cp, "$name found in clone");
    &delete($clone, $name);
    is(fetch($clone, $name), undef, "$name was deleted from clone");
}

is(count($clone), 0, "clone is now empty");
demolish($clone);

$count = 0;
my %seen;
my $iterator = first($hash);
while (!at_end($hash, $iterator)) {
    ++$count;
    my ($key, $value) = current($hash, $iterator);
    isnt($key, undef, "key at iterator $count is not undef");
    isnt($value, undef, "value at iterator $count is not undef");
    $seen{$key} = $value;
    $iterator = &next($hash, $iterator);
}
is ($count, scalar @unicode, "correct iteration count");
is_deeply(\%seen, \%want, "all the hash was seen");
ok(at_end($hash, end($hash)), "end() generates an iterator which is at_end()");
demolish($hash);

done_testing();
