#!usr/bin/perl
use strict;
use warnings;

use Test::More;
BEGIN { use_ok('Acme::ABH',
               qw(build insert demolish count first next current at_end fsck)) };

use FindBin qw($Bin);

my $hash = build();

my %to_cp;
my $count;

open my $fh, '<', "$Bin/Names";
while (<$fh>) {
    chomp;
    my ($cph, $name) = /^ *([A-F0-9]+) +([-A-Z0-9 ]+)/;
    die $_
        unless defined $name;
    my $cp = hex $cph;
    # We know U-0000 is not in this file;
    die $_
        unless $cp;
    $to_cp{$name} = $cp;
    insert($hash, $name, $cp);
    ++$count;
    is(count($hash), $count, "count is correct");
    my $counted;
    my %seen;
    my $iterator = first($hash);
    while (!at_end($hash, $iterator)) {
        ++$counted;
        my ($key, $value) = current($hash, $iterator);
        isnt($key, undef, "key at iterator $count for $name is not undef")
            if !defined $key;
        isnt($value, undef, "value at iterator $count for $name is not undef")
            if !defined $value;
        $seen{$key} = $value;
        $iterator = &next($hash, $iterator);
    }
    is ($count, $counted, "correct iteration count at $name");
    is_deeply(\%seen, \%to_cp, "all the hash was seen at $name");
}
is(fsck($hash), 0, "hash is not fscked");
demolish($hash);
done_testing();
